import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider extends ChangeNotifier {
  ///variable we need for selected theme
  late ThemeData _selectedTheme;

  ///constructor for for selecting dark or light theme
  ThemeProvider();
  ThemeProvider.setTheme({required bool isDark}) {
    _selectedTheme = isDark ? dark : light;
  }

  ///get theme method
  ThemeData get getTheme => _selectedTheme;

  ///method to change theme form dark to light and light to dark
  Future<void> swapTheme() async {
    var sharedPref = await SharedPreferences.getInstance();

    ///change theme value
    _selectedTheme = _selectedTheme == dark ? light : dark;

    ///false is dark true is light
    sharedPref.setBool('theme', _selectedTheme == dark ? false : true);
    notifyListeners();
  }

  ///fetch theme in app first start
  Future<void> fetchTheme() async {
    var sharedPref = await SharedPreferences.getInstance();

    ///if the pref value is null it shows that it is the first start of the app an theme has not been set so we set it as a light theme
    _selectedTheme = sharedPref.getBool('theme') == null
        ? light
        : sharedPref.getBool('theme') == false
        ? dark
        : light;
    notifyListeners();
  }

  ///Light theme data
  ThemeData light = ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: const Color(0xff65b3f8),
    backgroundColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    splashColor: const Color(0xffC7C7CC),
    colorScheme: const ColorScheme.light().copyWith(
      primary: const Color(0xff65b3f8),
      onSecondary: const Color(0xff334148),
      secondary: const Color(0xffC7C7CC),
      background: Colors.greenAccent,
      onBackground: const Color(0xffD0112D),
    ),
    indicatorColor: const Color(0xff334148),
    iconTheme: const IconThemeData(
      color: Color(0xffC7C7CC),
      size: 22,
    ),
    bottomSheetTheme: const BottomSheetThemeData(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(38.0), topRight: Radius.circular(38.0)),
      ),
      elevation: 20,
    ),
    appBarTheme: const AppBarTheme(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        titleTextStyle: TextStyle(
            color: Color(0xff333333),
            fontSize: 18,
            fontFamily: 'Sen',
            fontWeight: FontWeight.w700),
        iconTheme: IconThemeData(
          color: Color(0xff333333),
        )),
    textTheme: const TextTheme(
      headline1: TextStyle(
        color: Color(0xff65b3f8),
        fontWeight: FontWeight.w700,
        fontSize: 64,
        fontFamily: 'Sen',
      ),
      headline2: TextStyle(
        color: Color(0xff334148),
        fontWeight: FontWeight.w700,
        fontSize: 34,
        fontFamily: 'Sen',
      ),
      headline3: TextStyle(
        color: Color(0xff334148),
        fontWeight: FontWeight.w700,
        fontSize: 22,
        fontFamily: 'Sen',
      ),
      headline4: TextStyle(
        color: Color(0xff334148),
        fontWeight: FontWeight.w700,
        fontSize: 18,
        fontFamily: 'Sen',
      ),
      bodyText1: TextStyle(
        color: Color(0xff334148),
        fontSize: 18,
        fontFamily: 'Sen',
      ),
      subtitle1: TextStyle(
        color: Color(0xff334148),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      overline: TextStyle(
        color: Color(0xff333333),
        fontSize: 12,
        fontFamily: 'Sen',
      ),
      caption: TextStyle(
        color: Color(0xff334148),
        fontSize: 12,
        fontFamily: 'Sen',
      ),
      button: TextStyle(
        color: Color(0xff334148),
        fontWeight: FontWeight.w600,
        fontSize: 18,
        fontFamily: 'Sen',
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        foregroundColor:
        MaterialStateProperty.all<Color>(const Color(0xff120407)),
        backgroundColor:
        MaterialStateProperty.all<Color>(const Color(0xffffffff)),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
        ),
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(
          const Color(0xff65b3f8),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
            )),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      hoverColor: const Color(0xffB1B1B1),
      focusColor: const Color(0xffB1B1B1),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xffe5e5ea), width: 1),
        borderRadius: BorderRadius.circular(10.0),
      ),
      hintStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xffB1B1B1), width: 1.5),
        borderRadius: BorderRadius.circular(25.0),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xffB1B1B1), width: 1),
        borderRadius: BorderRadius.circular(25.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xffE5E5EA), width: 1),
        borderRadius: BorderRadius.circular(25.0),
      ),
      counterStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      helperStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      labelStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      contentPadding:
      const EdgeInsets.symmetric(horizontal: 14.0, vertical: 15),
    ),
  );

  ///Dark theme data
  ThemeData dark = ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: const Color(0xff181818),
    backgroundColor: const Color(0xff334148),
    scaffoldBackgroundColor: const Color(0xff334148),
    splashColor: const Color(0xf00a0a0a),
    colorScheme: const ColorScheme.light().copyWith(
      primary: const Color(0xffFF4D5B),
      onSecondary: Colors.white,
      secondary: const Color(0xffC7C7CC),
      background: const Color(0xff333333),
      onBackground: const Color(0xf00a0a0a),
    ),
    indicatorColor: const Color(0xffD8D8D8),
    inputDecorationTheme: InputDecorationTheme(
      hoverColor: const Color(0xffB1B1B1),
      focusColor: const Color(0xffB1B1B1),
      border: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xff606060), width: 1),
        borderRadius: BorderRadius.circular(10.0),
      ),
      hintStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xffB1B1B1), width: 1.5),
        borderRadius: BorderRadius.circular(10.0),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xff606060), width: 1),
        borderRadius: BorderRadius.circular(10.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Color(0xff606060), width: 1),
        borderRadius: BorderRadius.circular(10.0),
      ),
      counterStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      helperStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      labelStyle: const TextStyle(
        color: Color(0xffB1B1B1),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      contentPadding:
      const EdgeInsets.symmetric(horizontal: 14.0, vertical: 15),
    ),
    iconTheme: const IconThemeData(
      color: Color(0xffD8D8D8),
      size: 22,
    ),
    bottomSheetTheme: const BottomSheetThemeData(
      backgroundColor: Color(0xff334148),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(38.0), topRight: Radius.circular(38.0)),
      ),
      elevation: 20,
    ),
    appBarTheme: const AppBarTheme(
        backgroundColor: Color(0xff334148),
        elevation: 0,
        centerTitle: true,
        titleTextStyle: TextStyle(
            color: Color(0xffD8D8D8),
            fontSize: 18,
            fontFamily: 'Sen',
            fontWeight: FontWeight.w700),
        iconTheme: IconThemeData(
          color: Color(0xffD8D8D8),
        )),
    textTheme: const TextTheme(
      headline1: TextStyle(
        color: Color(0xffFF4D5B),
        fontWeight: FontWeight.w700,
        fontSize: 64,
        fontFamily: 'Sen',
      ),
      headline2: TextStyle(
        color: Color(0xffFF4D5B),
        fontWeight: FontWeight.w700,
        fontSize: 34,
        fontFamily: 'Sen',
      ),
      headline3: TextStyle(
        color: Color(0xffD8D8D8),
        fontWeight: FontWeight.w700,
        fontSize: 22,
        fontFamily: 'Sen',
      ),
      headline4: TextStyle(
        color: Color(0xffD8D8D8),
        fontWeight: FontWeight.w700,
        fontSize: 18,
        fontFamily: 'Sen',
      ),
      bodyText1: TextStyle(
        color: Color(0xffD8D8D8),
        fontSize: 18,
        fontFamily: 'Sen',
      ),
      subtitle1: TextStyle(
        color: Color(0xffD8D8D8),
        fontSize: 16,
        fontFamily: 'Sen',
      ),
      overline: TextStyle(
        color: Color(0xffD8D8D8),
        fontSize: 12,
        fontFamily: 'Sen',
      ),
      caption: TextStyle(
        color: Color(0xffD8D8D8),
        fontSize: 12,
        fontFamily: 'Sen',
      ),
      button: TextStyle(
        color: Color(0xffD8D8D8),
        fontWeight: FontWeight.w700,
        fontSize: 18,
        fontFamily: 'Sen',
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        foregroundColor:
        MaterialStateProperty.all<Color>(const Color(0xff120407)),
        backgroundColor: MaterialStateProperty.all<Color>(
          const Color(0xffD8D8D8),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
          padding:
          MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
          foregroundColor: MaterialStateProperty.all<Color>(
            Color(0xff334148),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
            const Color(0xffFF4D5B),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ))),
    ),
  );
}