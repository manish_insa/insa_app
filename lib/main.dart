
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:insurance_samadhan/providers/providers.dart';
import 'package:insurance_samadhan/providers/theme/theme_provider.dart';
import 'package:provider/provider.dart';
import 'app_data/routers.dart';

// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:firebase_auth/firebase_auth.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // var prefs = await SharedPreferences.getInstance();
  // prefs.clear();

  ///force device orientation to portraitUp
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      ///theme light / dark observer
      ChangeNotifierProvider(
        create: (BuildContext context) => ThemeProvider.setTheme(isDark: true),
        child: MyApp(),
      ),
    );
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      // key: ValueKey("appProvidersDefaultKey"),
      //provider initializing
      providers: providers,
      child: FutureBuilder(
        future: Provider.of<ThemeProvider>(context, listen: false).fetchTheme(),
        builder: (context, snapshot) => Consumer<ThemeProvider>(
          builder: (context, themeProvider, child) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: themeProvider.getTheme, //light theme
              /// initial route to '/' splash screen
              initialRoute: '/',
              onGenerateRoute: Routers.generateRoute,
            );
          },
        ),
      ),
    );
  }
}