import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  final _email = TextEditingController();
  final _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: BackButton(
          color: Colors.grey[700],
          onPressed: (){},
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            SizedBox(
              height: height * 0.05,
            ),
            Text(
              "Sign up",
              style: theme.textTheme.headline2,
            ),
            SizedBox(
              height: height * 0.01,
            ),
            Text(
              "Enter your Email to continue",
              style: theme.textTheme.bodyText1,
            ),
            SizedBox(
              height: height * 0.04,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Email Address",
                style: theme.textTheme.overline,
              ),
            ),
            SizedBox(
              height: height * 0.005,
            ),
            TextFormField(
              controller: _email,
              decoration: InputDecoration(
                hintText: 'Email',
              ),
              validator: (val) {
                if (val!.isEmpty) {
                  return 'Email can\'t be empty';
                }
                if (!val.contains('@')) {
                  return 'Invalid Email';
                } else {
                  return null;
                }
              },
            ),
            SizedBox(
              height: height * 0.01,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "  We will send an email verification link",
                style:
                theme.textTheme.caption?.copyWith(
                  color: Color(0xff8E8E93),
                ),
              ),
            ),
            SizedBox(
              height: height * 0.03,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Password",
                style: theme.textTheme.overline,
              ),
            ),
            SizedBox(
              height: height * 0.005,
            ),
            TextFormField(
              controller: _password,
              obscureText: hidePassword,
              decoration: InputDecoration(
                hintText: 'Password',
                suffixIcon: IconButton(
                  icon: Icon(
                    // Based on passwordVisible state choose the icon
                    hidePassword
                        ? Icons.visibility
                        : Icons.visibility_off,
                    // color: AppColors.lightGreyBlueColor,
                  ),
                  onPressed: () {
                    // Update the state i.e. toggle the state of passwordVisible variable
                    setState(() {
                      hidePassword = !hidePassword;
                    });
                  },
                ),
              ),
              validator: (val) {
                if (val!.isEmpty) {
                  return 'Password can\'t be empty';
                }
                if (!val
                    .contains(RegExp(r'[a-zA-Z]+'))) {
                  return 'Must contain atleast one alphabet character.';
                }
                if (!val.contains(RegExp(r'[0-9]+'))) {
                  return 'Must contain atleast one number.';
                }
                if (!val.contains(
                    RegExp(r'^(?=.*?[!@#\$&*~])+'))) {
                  return 'Must contain atleast one special character.';
                }
                if (val.length < 8) {
                  return 'Password must be at least 8 characters';
                } else {
                  return null;
                }
              },
            ),
            SizedBox(
              height: height * 0.02,
            ),
            SizedBox(
              width: double.infinity,
              height: 44,
              child: TextButton(
                onPressed: () {   },
                child:const Text(
                  'Continue',
                ),),),
            SizedBox(
              height: height * 0.02,
            ),
            Align(
              alignment: Alignment.center,
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Already have an account? ',
                      style: theme.textTheme.subtitle1,
                    ),
                    TextSpan(
                      text: ' Sign In',
                      style: theme.textTheme.subtitle1?.copyWith(
                        color: theme.colorScheme.primary,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.pushReplacementNamed(
                              context, '/sign_in');
                        },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
