import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:insurance_samadhan/providers/theme/theme_provider.dart';
import 'package:insurance_samadhan/screens/registration/registration_widgets/signin_bottom_sheet.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class SignIn extends StatefulWidget {
  // ignore: use_key_in_widget_constructors
  const SignIn({this.onPressBackButton});

  // ignore: prefer_typing_uninitialized_variables
  final onPressBackButton;

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  bool hidePassword = true;
  final _signInKey = GlobalKey<FormState>();
  bool loading = false;
  bool error = false;

  final buttonTextStyle = const TextStyle(
      fontSize: 17.0, color: Color(0xFF575757), fontWeight: FontWeight.w600);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      // appBar: AppBar(
      //   elevation: 0,
      //   leading: BackButton(
      //     color: Colors.grey[700],
      //     onPressed: widget.onPressBackButton,
      //   ),
      //   backgroundColor: Colors.transparent,
      // ),
      body: SingleChildScrollView(
        child: Form(
          key: _signInKey,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: height * 0.1,
                ),
                Text(
                  "Login Account",
                  style: theme.textTheme.headline2,
                ),
                SizedBox(
                  height: height * 0.03,
                ),
                Text(
                  "Phone number",
                  style: theme.textTheme.bodyText1,
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                TextFormField(
                  controller: _email,
                  decoration: const InputDecoration(
                    hintText: '+91-9876543210',
                    prefixIcon: Icon(Icons.phone_android),
                  ),
                ),
                SizedBox(
                  height: height * 0.04,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 44,
                  child: ElevatedButton(
                    onPressed: () {
                      Provider.of<ThemeProvider>(context, listen: false)
                          .swapTheme();
                    },

                    child: const Text(
                      'REQUEST OTP',
                    ),
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all<Color>(const Color(0xffffffff)),
                      backgroundColor:
                      MaterialStateProperty.all<Color>(const Color(0xff65b3f8)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                  Container(height: 1, width: width*0.3, color: Colors.grey,
                    ),

                  Text('Login With',style: theme.textTheme.overline,),

                  Container(height: 1, width: width*0.3, color: Colors.grey,
                    ),
                ]),
                SizedBox(
                  height: height * 0.04,
                ),

                SizedBox(
              height: 44,
              width: double.infinity,
              child: ElevatedButton.icon(
                icon: Image.asset(
                  "assets/icons/google.png",
                  height: 30,
                ),
                label: Text(
                  "Continue with Google",
                  style: buttonTextStyle,
                ),
                style: ButtonStyle(
                  foregroundColor:
                  MaterialStateProperty.all<Color>(
                      Colors.black),
                  backgroundColor:
                  MaterialStateProperty.all<Color>(
                      const Color(0xFFFFFFFF)),
                  shape: MaterialStateProperty.all<
                      RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius:
                      BorderRadius.circular(14),
                    ),
                  ),
                ),
                onPressed: () {
                },
              ),
            ),
                SizedBox(
                  height: height * 0.02,
                ),
              ],
            ),
          ),
        ),
      ),

    );
  }

  Future<Timer> loadData() async {
    return Timer(const Duration(seconds: 4), onDoneLoading);
  }

  onDoneLoading() {
    setState(() {
      loading = false;
    });
    // Navigator.of(context)
    //     .pushReplacement(MaterialPageRoute(builder: (context) =>  OtpScreen(number: phone.text,)));
  }

  Future<Timer> showError() async {
    return Timer(const Duration(seconds: 3), onDoneShowError);
  }

  onDoneShowError() {
    setState(() {
      error = false;
    });
  }
}
