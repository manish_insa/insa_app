import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


// ignore: use_key_in_widget_constructors
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  bool isLoading = true;

  @override
  void initState() {
    isLoading = false;
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/images/bg3.jpg'),
          fit: BoxFit.cover
          ),
          
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: ElasticInDown(
                child: ZoomIn(
                  duration: const Duration(seconds: 2),
                  child:
                  // Center(
                  //   child: SizedBox(
                  //       width: MediaQuery.of(context).size.width*0.4,
                  //     height: MediaQuery.of(context).size.width*0.4,
                  //
                  //   child: SvgPicture.asset(
                  //       'assets/icons/logo.svg',),
                  //   ),
                  // ),

                  Image.asset(
                    "assets/icons/logo.png",
                    width: MediaQuery.of(context).size.width*0.6,
                  ),
                ),
                duration: const Duration(seconds: 1),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<Timer> loadData() async {
    return Timer(const Duration(seconds: 3), onDoneLoading);
  }

  onDoneLoading() async {

    Navigator.pushReplacementNamed(context, '/sign_in');



  }


}