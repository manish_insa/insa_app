import 'package:flutter/material.dart';
import 'package:insurance_samadhan/screens/registration/signin.dart';
import 'package:insurance_samadhan/screens/registration/signup.dart';
import 'package:insurance_samadhan/screens/welcome/splash.dart';

class Routers {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) =>  SplashScreen());
      case '/sign_in':
        return MaterialPageRoute(builder: (_) => const SignIn());
      case '/sign_up':
        return MaterialPageRoute(builder: (_) => const SignUp());
      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}